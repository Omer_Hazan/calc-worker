import json
import os
import re
import sys
from enum import Enum

import logbook
import pika
import redis
from pydantic import BaseModel
from pika import BasicProperties
from pika.adapters.blocking_connection import BlockingChannel
from pika.spec import Basic

from calc_worker.conf import REDIS_URI_PATTERN, RABBIT_HOST_PATTERN, REDIS_KEY

# Setup logging
logbook.StreamHandler(sys.stdout).push_application()
logger = logbook.Logger("web_server")


class Operator(Enum):
    """
    Mathematical operator options
    """

    SUM = "sum"
    SUB = "sub"
    MUL = "mul"
    DIV = "div"


# Maps between Operator value and the associate mathematical operation
OPERATOR_TO_FUNC = {
    Operator.SUM: lambda a, b: a + b,
    Operator.SUB: lambda a, b: a - b,
    Operator.MUL: lambda a, b: a * b,
    Operator.DIV: lambda a, b: a / b,
}


class Message(BaseModel):
    """
    Model that represents a message structure
    """

    a: float
    b: float
    operator: Operator
    uid: str


class CalcWorker:
    """
    This class contains functionality for the CalcWorker including useful
    connections objects

    Args:
        redis_uri: The redis uri to connect to
        rabbit_host: The rabbit host to connect to

    Attributes:
        redis_pool: The redis's connection pool
        rabbit_channel: The rabbit's channel to consume

    """

    def __init__(self, redis_uri: str, rabbit_host: str):
        self.redis_pool = self.create_redis_pool(redis_uri)
        self.rabbit_channel = self.setup_channels(rabbit_host)

    @staticmethod
    def create_redis_pool(redis_uri: str):
        """
        Creates a Redis connection pool and test the connection to it

        Args:
            redis_uri: The redis's uri

        """
        redis_m = re.search(REDIS_URI_PATTERN, redis_uri)
        if redis_m is None:
            raise AttributeError("Redis URI is invalid")
        redis_pool = redis.ConnectionPool(
            host=redis_m.group("host"),
            port=redis_m.group("port"),
            db=redis_m.group("db"),
        )
        conn_test = redis.Redis(connection_pool=redis_pool)
        conn_test.ping()
        logger.debug("Successfully connected to Redis")
        return redis_pool

    @staticmethod
    def setup_channels(rabbit_host: str) -> BlockingChannel:
        """
        Setup the connections for the RabbitMQ and the calculate queue.

        Args:
            rabbit_host: The RabbitMQ's host to connect to

        Returns: The channel object created

        """
        rabbit_m = re.search(RABBIT_HOST_PATTERN, rabbit_host)
        if rabbit_m is None:
            raise AttributeError("Rabbit host is invalid")
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                rabbit_m.group("host"), int(rabbit_m.group("port"))
            )
        )

        channel = connection.channel()
        channel.confirm_delivery()
        channel.queue_declare(
            queue="calculate", durable=True, auto_delete=False
        )
        return channel

    def on_message(
        self,
        ch: BlockingChannel,
        method: Basic.Deliver,
        props: BasicProperties,
        body: bytes,
    ):
        """
        Method called when receiving a message in the queue
            1. Execute the associate operation for the operator and operands
                given in the body
            2. Add to redis's hash the uid from the body as key amd the result
                from the operation as value

        Args:
            ch: The channel the message received from
            method: The receiving method
            props: The message's properties
            body: The message contents

        """
        logger.debug(f"Message received: {str(body)}")
        message = Message(**json.loads(body.decode()))
        r = redis.Redis(connection_pool=self.redis_pool)
        result = OPERATOR_TO_FUNC[message.operator](message.a, message.b)
        r.hset(REDIS_KEY, message.uid, result)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def run(self):
        """
        Start consuming the Rabbitmq channel for messages
        """
        logger.info("Waiting for messages...")
        self.rabbit_channel.basic_consume(
            queue="calculate", on_message_callback=self.on_message
        )
        try:
            self.rabbit_channel.start_consuming()
        except KeyboardInterrupt:
            self.rabbit_channel.stop_consuming()


def main(redis_uri: str, rabbit_host: str):
    """
    Setup preparations and start consuming for calculations

    Args:
        redis_uri: The redis uri to connect to
        rabbit_host: The rabbit host to connect to

    """
    ttl_worker = CalcWorker(redis_uri, rabbit_host)
    logger.info("Calc worker started")
    ttl_worker.run()


def init():
    """
    Execute the main and log exceptions
    """
    if __name__ == "__main__":
        try:
            redis_uri = os.environ.get("REDIS", "localhost:6379/0")
            rabbit_host = os.environ.get("RABBIT", "localhost:5672")
            main(redis_uri, rabbit_host)
        except Exception:
            logger.exception("Calc worker failure")


init()
