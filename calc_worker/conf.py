# Redis uri string pattern
REDIS_URI_PATTERN = "(?P<host>[^:/ ]+).?(?P<port>[0-9]*).*/(?P<db>[^:/ ]+)"

# Rabbit host string pattern
RABBIT_HOST_PATTERN = "(?P<host>[^:/ ]+).?(?P<port>[0-9]*).*"

# Redis hash key name
REDIS_KEY = "calc_results"
