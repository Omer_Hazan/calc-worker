import json

import pika
import pytest
import redis

from calc_worker import calc_worker
from calc_worker.conf import REDIS_KEY


@pytest.fixture
def calcworker(mocker):
    """
    Fixture for CalcWorker class that mocks all connections
    """
    redis_mock = mocker.Mock()
    rabbit_mock = mocker.Mock()
    mocker.patch.object(calc_worker.CalcWorker, "create_redis_pool",
                        return_value=redis_mock)
    mocker.patch.object(calc_worker.CalcWorker, "setup_channels",
                        return_value=rabbit_mock)
    worker = calc_worker.CalcWorker("localhost:1234/0", "localhost:121")
    return worker, redis_mock, rabbit_mock


@pytest.mark.parametrize("operator, expected",
                         [(calc_worker.Operator.SUM, 3),
                          (calc_worker.Operator.SUB, -1),
                          (calc_worker.Operator.MUL, 2),
                          (calc_worker.Operator.DIV, 0.5)])
def test_operators(operator, expected):
    """
    Test operators happy flow functionality
    """
    assert calc_worker.OPERATOR_TO_FUNC[operator](1, 2) == expected


def test_on_message_happy_flow(mocker, calcworker):
    """
   Test happy flow functionality for on_message
    """
    worker, _, _ = calcworker
    hset_mock = mocker.Mock()
    mocker.patch.object(redis.Redis, 'hset', hset_mock)
    body = json.dumps(dict(a=1, b=2, operator=calc_worker.Operator.SUM.value,
                           uid=1)).encode()
    worker.on_message(mocker.Mock(), mocker.Mock(), mocker.Mock(), body)
    hset_mock.assert_called_with(REDIS_KEY, "1", 3.0)


def test_setup_channels_happy_flow(mocker):
    """
    Test happy flow functionality for setup_channels
    """
    queue_declare_mock = mocker.Mock()
    channel_mock = mocker.Mock()
    queue_bind_mock = mocker.Mock()

    channel_mock.queue_declare = queue_declare_mock
    channel_mock.queue_bind = queue_bind_mock

    patcher = mocker.patch.object(pika, 'BlockingConnection')
    mocker.patch.object(pika, 'ConnectionParameters')

    redis_mock = patcher.return_value
    redis_mock.channel.return_value = channel_mock

    calc_worker.CalcWorker.setup_channels('localhost:8000')

    queue_declare_mock.assert_called_with(queue="calculate", durable=True,
                                          auto_delete=False)


@pytest.mark.parametrize("pattern, exception", [('', AttributeError),
                                                ('localhost', AttributeError),
                                                ('1234', AttributeError)])
def test_redis_invalid_uri_pattern(pattern, exception):
    """
    Tests the case when giving setup_redis invalid argument
    """
    with pytest.raises(exception):
        calc_worker.CalcWorker.create_redis_pool(pattern)


@pytest.mark.parametrize("pattern, exception", [('', AttributeError),
                                                ('localhost', ValueError),
                                                ('1234', ValueError)])
def test_rabbit_invalid_uri_pattern(pattern, exception):
    """
    Tests the case when giving setup_rabbitmq invalid argument
    """
    with pytest.raises(exception):
        calc_worker.CalcWorker.setup_channels(pattern)


def test_create_redis_pool_happy_flow(mocker):
    """
    Test create_redis_pool happy flow functionality
    """
    mocker.patch.object(redis, 'ConnectionPool')
    patcher = mocker.patch.object(redis, 'Redis')
    redis_mock = patcher.return_value
    ping_mock = mocker.Mock()
    redis_mock.ping = ping_mock
    calc_worker.CalcWorker.create_redis_pool("localhost:8000/0")
    ping_mock.assert_called_once()


def test_main_happy_flow(calcworker):
    """
    Test main happy flow functionality
    """
    worker, _, rabbit_mock = calcworker
    calc_worker.main("a", "b")
    rabbit_mock.start_consuming.assert_called_once()


def test_main_keyboardinterrupt(calcworker):
    """
    Test main when KeyboardInterrupt raises
    """
    worker, _, rabbit_mock = calcworker
    rabbit_mock.start_consuming.side_effect = KeyboardInterrupt()
    calc_worker.main("a", "b")
    rabbit_mock.stop_consuming.assert_called_once()


def test_init(mocker):
    """
    Test module init (__main__)
    """
    mocker.patch.object(calc_worker, "__name__", "__main__")
    main_mock = mocker.patch.object(calc_worker, "main")
    calc_worker.init()
    main_mock.assert_called_once()


def test_init_exception(mocker):
    """
    Test logging when exception occurs in main
    """
    mocker.patch.object(calc_worker, "__name__", "__main__")
    mocker.patch.object(calc_worker, "main", side_effect=ValueError)

    logger_mock = mocker.patch.object(calc_worker.logger, "exception")
    calc_worker.init()
    logger_mock.assert_called_once()
